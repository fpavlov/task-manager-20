package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.IUserOwnedRepository;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 13.01.2022.
 */
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public void clear(final String userId) {
        List<M> items = this.findAll(userId);
        for (final M item : items) {
            this.remove(item);
        }
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        final List<M> filterItems = new ArrayList<>();
        for (final M item : this.findAll()) {
            if (userId.equals(item.getUserId())) filterItems.add(item);
        }
        return filterItems;
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        final List<M> items = this.findAll(userId);
        items.sort(comparator);
        return items;
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        final List<M> items = this.findAll(userId);
        items.sort(sort.getComparator());
        return items;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return this.findById(userId, id) != null;
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        M foundItem = null;
        for (final M item : this.findAll()) {
            if (userId.equals(item.getUserId()) && id.equals(item.getId())) {
                foundItem = item;
                break;
            }
        }
        return foundItem;
    }

    @Override
    public M findByIndex(final String userId, final Integer index) {
        if (userId == null || index == null || index < 0 || index > this.getSize(userId)) return null;
        return this.findAll(userId).get(index);
    }

    @Override
    public int getSize(final String userId) {
        return this.findAll(userId).size();
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M item = this.findById(userId, id);
        return this.remove(item);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || index == null || index < 0 || index > this.getSize(userId)) return null;
        final M item = this.findByIndex(userId, index);
        return this.remove(item);
    }

    @Override
    public M add(final String userId, final M item) {
        if (userId == null || item == null) return null;
        item.setUserId(userId);
        return this.add(item);
    }

    @Override
    public M remove(final String userId, final M item) {
        if (userId == null || item == null) return null;
        return this.removeById(userId, item.getId());
    }

}
