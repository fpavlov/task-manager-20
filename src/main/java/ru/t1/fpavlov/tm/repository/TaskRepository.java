package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.ITaskRepository;
import ru.t1.fpavlov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, final String name) {
        final Task item = new Task(name);
        item.setUserId(userId);
        return this.add(item);
    }

    @Override
    public Task create(final String userId, final String name, final String description) {
        final Task item = new Task(name, description);
        item.setUserId(userId);
        return this.add(item);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task item : this.items) {
            if (projectId.equals(item.getProjectId()) && userId.equals(item.getUserId()))
                result.add(item);
        }
        return result;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId, final Comparator comparator) {
        final List<Task> result = this.findAllByProjectId(userId, projectId);
        if (comparator != null) result.sort(comparator);
        return result;
    }

}
