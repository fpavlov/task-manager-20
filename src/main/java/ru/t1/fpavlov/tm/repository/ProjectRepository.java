package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.model.Project;

/*
 * Created by fpavlov on 10.10.2021.
 */
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project item = new Project(name);
        item.setUserId(userId);
        return this.add(item);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        final Project item = new Project(name, description);
        item.setUserId(userId);
        return this.add(item);
    }

}
