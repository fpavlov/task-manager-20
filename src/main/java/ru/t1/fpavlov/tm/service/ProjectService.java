package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.repository.IProjectRepository;
import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.fpavlov.tm.exception.field.DescriptionEmptyException;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.exception.field.NameEmptyException;
import ru.t1.fpavlov.tm.exception.user.UserIdEmptyException;
import ru.t1.fpavlov.tm.model.Project;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return this.repository.create(userId, name);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return this.repository.create(userId, name, description);
    }

    @Override
    public Project update(final String userId, final Project project, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project item = this.findById(userId, id);
        if (item == null) throw new ProjectNotFoundException();
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project item = this.findByIndex(userId, index);
        if (item == null) throw new ProjectNotFoundException();
        item.setName(name);
        item.setDescription(description);
        return item;
    }

    @Override
    public Project changeStatus(final String userId, final Project project, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        if (status == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) return null;
        final Project item = this.findById(userId, id);
        if (item == null) throw new ProjectNotFoundException();
        item.setStatus(status);
        return item;
    }

    @Override
    public Project changeStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= this.repository.getSize()) throw new IndexIncorrectException();
        if (status == null) return null;
        final Project item = this.findByIndex(userId, index);
        if (item == null) throw new ProjectNotFoundException();
        item.setStatus(status);
        return item;
    }

}
