package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.service.IAuthService;
import ru.t1.fpavlov.tm.api.service.IUserService;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.exception.field.PasswordEmptyException;
import ru.t1.fpavlov.tm.exception.user.AccessDeniedException;
import ru.t1.fpavlov.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.fpavlov.tm.exception.user.LoginEmptyException;
import ru.t1.fpavlov.tm.exception.user.PermissionException;
import ru.t1.fpavlov.tm.model.User;
import ru.t1.fpavlov.tm.util.HashUtil;

import java.util.Arrays;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        if (email == null || email.isEmpty()) return this.userService.create(login, password);
        return this.userService.create(login, password, email);
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = this.userService.findByLogin(login);
        final String passwordHash = HashUtil.salt(password);
        if (user == null || !passwordHash.equals(user.getPasswordHash())) {
            throw new IncorrectLoginOrPasswordException();
        }
        this.userId = user.getId();
    }

    @Override
    public void logout() {
        this.userId = null;
    }

    @Override
    public boolean isAuth() {
        return this.userId != null;
    }

    @Override
    public String getUserId() {
        if (!this.isAuth()) throw new AccessDeniedException();
        return this.userId;
    }

    @Override
    public User getUser() {
        if (!this.isAuth()) throw new AccessDeniedException();
        final User user = this.userService.findById(this.userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public void checkRoles(Role[] roles) {
        if (roles == null) return;
        final User user = this.getUser();
        final Role userRole = user.getRole();
        if (userRole == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionException();
    }

}
