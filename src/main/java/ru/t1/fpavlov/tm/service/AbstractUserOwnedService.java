package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.repository.IUserOwnedRepository;
import ru.t1.fpavlov.tm.api.service.IUserOwnedService;
import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.exception.field.IdEmptyException;
import ru.t1.fpavlov.tm.exception.field.IndexIncorrectException;
import ru.t1.fpavlov.tm.exception.user.UserIdEmptyException;
import ru.t1.fpavlov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

/**
 * Created by fpavlov on 14.01.2022.
 */
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(final R repository) {
        super(repository);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        this.repository.clear(userId);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.existsById(userId, id);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return this.repository.findAll(userId);
    }

    @Override
    public List<M> findAll(final String userId, final Comparator comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return this.repository.findAll(userId);
        return this.repository.findAll(userId, comparator);
    }

    public List<M> findAll(final String userId, final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return this.repository.findAll(userId);
        return this.repository.findAll(userId, sort);
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.findById(userId, id);
    }

    @Override
    public M findByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > this.repository.getSize(userId))
            throw new IndexIncorrectException();
        return this.repository.findByIndex(userId, index);
    }

    @Override
    public int getSize(final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return this.repository.getSize(userId);
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return this.repository.removeById(userId, id);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index > this.repository.getSize(userId))
            throw new IndexIncorrectException();
        return this.repository.removeByIndex(userId, index);
    }

    @Override
    public M add(final String userId, final M item) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (item == null) return null;
        return this.repository.add(userId, item);
    }

    @Override
    public M remove(final String userId, final M item) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (item == null) return null;
        return this.repository.remove(userId, item);
    }

}
