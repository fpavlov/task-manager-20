package ru.t1.fpavlov.tm.exception.user;

/**
 * Created by fpavlov on 14.01.2022.
 */
public class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("Error! User id is empty");
    }

}
