package ru.t1.fpavlov.tm.exception.field;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class ProjectIdEmptyException extends AbstractFieldException {

    public ProjectIdEmptyException() {
        super("Error! Project id is empty");
    }

}
