package ru.t1.fpavlov.tm.api.repository;

import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 19.12.2021.
 */
public interface IUserRepository extends IRepository<User> {

    User create(final String login, final String password);

    User create(final String login, final String password, final String email);

    User create(final String login, final String password, final Role role);

    User findByLogin(final String login);

    User findByEmail(final String email);

    Boolean isLoginExist(final String login);

    Boolean isEmailExist(final String email);

}
