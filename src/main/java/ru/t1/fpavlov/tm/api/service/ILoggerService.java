package ru.t1.fpavlov.tm.api.service;

/**
 * Created by fpavlov on 07.12.2021.
 */
public interface ILoggerService {

    void info(final String message);

    void debug(final String message);

    void command(final String message);

    void error(final Exception e);

}
