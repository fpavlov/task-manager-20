package ru.t1.fpavlov.tm.api.model;

import ru.t1.fpavlov.tm.enumerated.Status;

/**
 * Created by fpavlov on 26.11.2021.
 */
public interface IHasStatus {

    Status getStatus();

    void setStatus(final Status status);

}
