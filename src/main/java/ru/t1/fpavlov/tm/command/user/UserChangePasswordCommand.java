package ru.t1.fpavlov.tm.command.user;

import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 21.12.2021.
 */
public final class UserChangePasswordCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Change password";

    public static final String NAME = "change-password";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final User user = this.getAuthService().getUser();
        final String login = user.getLogin();
        final String oldPassword = this.input("Old password:");
        this.getAuthService().login(login, oldPassword);
        final String newPassword = this.input("New password:");
        final String passwordConfirmation = this.input("New password again:");
        if (newPassword != null && passwordConfirmation != null &&
                !oldPassword.equals(newPassword) && newPassword.equals(passwordConfirmation)) {
            this.getServiceLocator().getUserService().setPassword(user.getId(), newPassword);
        }
    }

}
