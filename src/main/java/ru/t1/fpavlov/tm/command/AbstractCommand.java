package ru.t1.fpavlov.tm.command;

import ru.t1.fpavlov.tm.api.model.ICommand;
import ru.t1.fpavlov.tm.api.service.IServiceLocator;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 07.12.2021.
 */
public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract String getName();

    public abstract Role[] getRoles();

    public final IServiceLocator getServiceLocator() {
        return this.serviceLocator;
    }

    public final void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public final String getUserId() {
        return this.getServiceLocator().getAuthService().getUserId();
    }

    @Override
    public String toString() {
        final String name = this.getName();
        final String description = this.getDescription();
        final String argument = this.getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
