package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.api.service.ITaskService;
import ru.t1.fpavlov.tm.command.entity.AbstractEntityCommand;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public abstract class AbstractTaskCommand extends AbstractEntityCommand {

    protected final ITaskService getTaskService() {
        return this.getServiceLocator().getTaskService();
    }

    protected final Task findById() {
        System.out.println("Enter task id");
        final String itemId = TerminalUtil.nextLine();
        final String userId = this.getUserId();
        return this.getTaskService().findById(userId, itemId);
    }

    protected final Task findByIndex() {
        System.out.println("Enter task index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final String userId = this.getUserId();
        return this.getTaskService().findByIndex(userId, itemIndex - 1);
    }

    protected final void renderEntities(final List<Task> entities, final String listName) {
        if (listName != null) System.out.println(listName);
        for (int i = 0; i < entities.size(); i++) {
            System.out.format("|%2d%s%n", i + 1, entities.get(i));
        }
    }

}
