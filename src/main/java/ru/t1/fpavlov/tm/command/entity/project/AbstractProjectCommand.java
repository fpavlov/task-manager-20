package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.api.service.IProjectService;
import ru.t1.fpavlov.tm.command.entity.AbstractEntityCommand;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public abstract class AbstractProjectCommand extends AbstractEntityCommand {

    protected final IProjectService getProjectService() {
        return this.getServiceLocator().getProjectService();
    }

    protected final Project findById() {
        System.out.println("Enter project id");
        final String itemId = TerminalUtil.nextLine();
        final String userId = this.getUserId();
        return this.getProjectService().findById(userId, itemId);
    }

    protected final Project findByIndex() {
        System.out.println("Enter project index");
        final Integer itemIndex = TerminalUtil.nextInteger();
        final String userId = this.getUserId();
        return this.getProjectService().findByIndex(userId, itemIndex - 1);
    }

    protected final void renderEntities(final List<Project> entities, final String listName) {
        if (listName != null) System.out.println(listName);
        for (int i = 0; i < entities.size(); i++) {
            System.out.format("|%2d%s%n", i + 1, entities.get(i));
        }
    }

}
