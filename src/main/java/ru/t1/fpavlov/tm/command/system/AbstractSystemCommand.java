package ru.t1.fpavlov.tm.command.system;

import ru.t1.fpavlov.tm.api.service.ICommandService;
import ru.t1.fpavlov.tm.command.AbstractCommand;
import ru.t1.fpavlov.tm.enumerated.Role;

/**
 * Created by fpavlov on 07.12.2021.
 */
public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return this.serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
